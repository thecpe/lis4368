> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Christian Pelaez-Espinosa

### Project 1 Requirements:

*Three Parts*


1. Create repositories for lis4368p1
2. Push project to bitbucket

#### README.md file should include the following items:

* Deliverables: 

1. Provide Bitbucket read-only access to p1 repo, *must* include README.md, using Markdown syntax.
2. Blackboard Links: p1 Bitbucket repo


> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>

#### Project Screenshots:

*Screenshot of P1*:


![Pass.png](https://bitbucket.org/repo/KxEpGk/images/2251137766-Pass.png)
![Fail.png](https://bitbucket.org/repo/KxEpGk/images/539546842-Fail.png)